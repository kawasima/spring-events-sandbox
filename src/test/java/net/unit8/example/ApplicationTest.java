package net.unit8.example;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;
import net.unit8.example.application.*;
import net.unit8.example.domain.Issue;
import net.unit8.example.domain.IssueId;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;

import static org.mockito.Mockito.*;

class ApplicationTest {
    ReportLimitedPort reportLimitedPort;
    GenericApplicationContext context;

    @BeforeEach
    void setup() {
        GetIssuePort getIssuePort = mock(GetIssuePort.class);
        when(getIssuePort.find(any())).thenReturn(new Issue(IssueId.create()));

        SaveIssuePort saveIssuePort = mock(SaveIssuePort.class);
        reportLimitedPort = mock(ReportLimitedPort.class);
        NotifyToSlackPort notifyToSlackPort = mock(NotifyToSlackPort.class);

        context = new AnnotationConfigApplicationContext();
        context.registerBean(ApplicationEventPublisher.class, () -> context);

        // Register UseCase
        context.registerBean(CreateIssueUseCase.class);
        context.registerBean(RejectIssueUseCase.class);
        context.registerBean(NotifyToSlackUseCase.class);
        context.registerBean(ReportLimitedUseCase.class);

        // Register mock ports
        context.registerBean(GetIssuePort.class, () -> getIssuePort);
        context.registerBean(SaveIssuePort.class, () -> saveIssuePort);
        context.registerBean(ReportLimitedPort.class, () -> reportLimitedPort);
        context.registerBean(NotifyToSlackPort.class, () -> notifyToSlackPort);

        context.refresh();
    }

    @Test
    void test() {
        CreateIssueUseCase createIssueUseCase = context.getBean(CreateIssueUseCase.class);
        createIssueUseCase.handle();
        RejectIssueUseCase rejectIssueUseCase = context.getBean(RejectIssueUseCase.class);
        rejectIssueUseCase.handle(new RejectIssueCommand(NanoIdUtils.randomNanoId()));
        verify(reportLimitedPort).handle(eq("CREATED ISSUE"), ArgumentMatchers.any());
        verify(reportLimitedPort).handle(eq("REJECTED ISSUE"), ArgumentMatchers.any());
    }

}