package net.unit8.example.domain;

import com.aventrix.jnanoid.jnanoid.NanoIdUtils;

public record IssueId(String value) {
    public static IssueId create() {
        return new IssueId(NanoIdUtils.randomNanoId());
    }
}
