package net.unit8.example.domain;

public record CreatedIssueEvent(IssueId issueId) {
}
