package net.unit8.example.domain;

public record RejectedIssueEvent(IssueId issueId) {

}
