package net.unit8.example.application;

import net.unit8.example.domain.CreatedIssueEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class NotifyToSlackUseCase {
    private final NotifyToSlackPort notifyToSlackPort;

    public NotifyToSlackUseCase(NotifyToSlackPort notifyToSlackPort) {
        this.notifyToSlackPort = notifyToSlackPort;
    }

    @Async
    @EventListener
    public void handle(CreatedIssueEvent event) {
        notifyToSlackPort.notify(event.issueId());
    }
}
