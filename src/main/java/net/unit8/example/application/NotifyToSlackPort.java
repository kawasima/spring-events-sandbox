package net.unit8.example.application;

import net.unit8.example.domain.IssueId;

public interface NotifyToSlackPort {
    void notify(IssueId issueId);
}
