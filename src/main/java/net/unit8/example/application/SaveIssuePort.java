package net.unit8.example.application;

import net.unit8.example.domain.Issue;

public interface SaveIssuePort {
    void save(Issue issue);
}
