package net.unit8.example.application;

import net.unit8.example.domain.CreatedIssueEvent;
import net.unit8.example.domain.RejectedIssueEvent;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

@Component
public class ReportLimitedUseCase {
    private final ReportLimitedPort reportLimitedPort;

    public ReportLimitedUseCase(ReportLimitedPort reportLimitedPort) {
        this.reportLimitedPort = reportLimitedPort;
    }

    private void report(String eventType, String name) {
        reportLimitedPort.handle(eventType, name);
    }

    @Async
    @EventListener
    public void handleCreatedIssueEvent(CreatedIssueEvent event) {
        report("CREATED ISSUE", event.issueId().value());
    }

    @Async
    @EventListener
    public void handleRejectedIssueEvent(RejectedIssueEvent event) {
        report("REJECTED ISSUE", event.issueId().value());
    }
}
