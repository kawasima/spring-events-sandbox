package net.unit8.example.application;

import net.unit8.example.domain.Issue;
import net.unit8.example.domain.IssueId;

public interface GetIssuePort {
    Issue find(IssueId issueId);
}
