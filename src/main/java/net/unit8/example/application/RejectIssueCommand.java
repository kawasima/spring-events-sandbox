package net.unit8.example.application;

public record RejectIssueCommand(String issueId) {

}
