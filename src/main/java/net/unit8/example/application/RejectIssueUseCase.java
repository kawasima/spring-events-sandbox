package net.unit8.example.application;

import net.unit8.example.domain.Issue;
import net.unit8.example.domain.IssueId;
import net.unit8.example.domain.RejectedIssueEvent;
import org.springframework.context.ApplicationEventPublisher;

public class RejectIssueUseCase {
    private final GetIssuePort getIssuePort;
    private final SaveIssuePort saveIssuePort;
    private final ApplicationEventPublisher eventPublisher;

    public RejectIssueUseCase(GetIssuePort getIssuePort, SaveIssuePort saveIssuePort, ApplicationEventPublisher eventPublisher) {
        this.getIssuePort = getIssuePort;
        this.saveIssuePort = saveIssuePort;
        this.eventPublisher = eventPublisher;
    }

    public void handle(RejectIssueCommand command) {
        Issue issue = getIssuePort.find(new IssueId(command.issueId()));
        saveIssuePort.save(issue);
        RejectedIssueEvent rejectedIssueEvent = new RejectedIssueEvent(issue.issueId());
        eventPublisher.publishEvent(rejectedIssueEvent);
    }
}
