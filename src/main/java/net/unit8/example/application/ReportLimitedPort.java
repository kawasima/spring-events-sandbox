package net.unit8.example.application;

public interface ReportLimitedPort {
    void handle(String eventType, String name);
}
