package net.unit8.example.application;

import net.unit8.example.domain.CreatedIssueEvent;
import net.unit8.example.domain.Issue;
import net.unit8.example.domain.IssueId;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

@Component
public class CreateIssueUseCase {
    private final ApplicationEventPublisher eventPublisher;

    public CreateIssueUseCase(ApplicationEventPublisher eventPublisher) {
        this.eventPublisher = eventPublisher;
    }

    public void handle() {
        IssueId issueId = IssueId.create();
        Issue issue = new Issue(issueId);
        CreatedIssueEvent createdIssueEvent = new CreatedIssueEvent(issue.issueId());
        eventPublisher.publishEvent(createdIssueEvent);
    }
}
